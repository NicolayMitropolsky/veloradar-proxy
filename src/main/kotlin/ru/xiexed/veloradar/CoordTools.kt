package ru.xiexed.veloradar

import org.geotools.referencing.CRS


val sourceCRS = CRS.decode("EPSG:3857")!! // WGS 84
val targetCRS = CRS.decode("EPSG:3395")!!  // WGS 84 / World Mercator

val transform = CRS.findMathTransform(sourceCRS, targetCRS)

fun getTileNumber(lat: Double, lon: Double, zoom: Int): Pair<Double, Double> {
    var xtile = ((lon + 180) / 360 * (1 shl zoom))
    var ytile = ((1 - Math.log(Math.tan(Math.toRadians(lat)) + 1 / Math.cos(Math.toRadians(lat))) / Math.PI) / 2 * (1 shl zoom))
//    if (xtile < 0)
//        xtile = 0
//    if (xtile >= 1 shl zoom)
//        xtile = (1 shl zoom) - 1
//    if (ytile < 0)
//        ytile = 0
//    if (ytile >= 1 shl zoom)
//        ytile = (1 shl zoom) - 1
    return Pair(xtile, ytile)
}


fun main(args: Array<String>) {

    val dst = FloatArray(2)
    val src = floatArrayOf(37.1f, 54.2f)
    transform.transform(src, 0, dst, 0, 1 )

    println("dst = " + dst.joinToString())

}
