package ru.xiexed.veloradar.trackproxy

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheBuilderSpec
import com.google.common.cache.CacheLoader
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.core.io.PathResource
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.core.io.buffer.DataBufferFactory
import org.springframework.core.io.buffer.DataBufferUtils
import org.springframework.core.io.buffer.DefaultDataBufferFactory
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyExtractors
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.router

import org.springframework.web.reactive.function.server.ServerResponse.*
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import reactor.core.publisher.UnicastProcessor
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.concurrent.TimeUnit


@SpringBootApplication
class RadarProxyApplication {

    private val sampleFile = "/home/nickl/Downloads/pm-9902-5207.png"

    //http://get.veloradar.ru/tiles/report/4/14/2/pm-9902-5207.png

    @Bean
    fun dataBufferFactory(): DataBufferFactory = DefaultDataBufferFactory()


    @Bean
    fun websiteRouter(tilesCache: TilesCache) = router {
//        GET("/tracks/{z}/{x}/{y}.png", { request ->
//            val (z, x, y) = listOf("z", "x", "y").map { request.pathVariable(it) }
//            println("req $z $x $y")
//            ok().body(BodyInserters.fromResource(PathResource(sampleFile)))
//        })
        GET("/tracks/{z}/{x}/{y}.png", { request ->
            val (z, x, y) = listOf("z", "x", "y").map { request.pathVariable(it).toInt() }
            println("req $z $x $y")
            ok().body(tilesCache.tile(z, x, y))


        })
    }

}

@Component
class TilesCache(private val dataBufferFactory: DataBufferFactory) {

    val storageDir = "tilesCache"

    private val client = WebClient.create("http://get.veloradar.ru")

    private val cache = CacheBuilder.newBuilder()
            .expireAfterAccess(10, TimeUnit.MINUTES)
            .build<Location, Flux<DataBuffer>>(
                    CacheLoader.from({ input: Location? ->
                        load(input!!)
                    })
            )

    private fun load(l: Location): Flux<DataBuffer> {
        val (z, x, y) = l
        val path = Paths.get(storageDir, z.toString(), x.toString(), "$y.png")
        val pathResource = PathResource(path)
        if (pathResource.exists())
            return Flux.defer {
                DataBufferUtils.read(pathResource, dataBufferFactory, 1024).cache()
            }

        Files.createDirectories(path.parent)
        Files.createFile(path)

        val veloradarData = client.get()
                .uri("/tiles/report/4/{z}/{c}/pm-{x}-{y}.png", z, x.rem(100), x, y)
                .exchange()
                .flatMapMany { it.body(BodyExtractors.toDataBuffers()) }

//        veloradarData.subscribe({
//            println("veloradar $it")
//        })

        val writableChannel = pathResource.writableChannel()
        val saveFile = DataBufferUtils.write(veloradarData, writableChannel)
        val fileWrite = saveFile.doOnNext({
            println("releasing ${it}")
            DataBufferUtils.release(it)
        }).doOnComplete {
            println("closing")
            writableChannel.close()
        }

        val mono = fileWrite.then().cache().log("mono")

        return mono.thenMany(Flux.defer {

            println("flatmapmany")

            val pathResource1 = PathResource(path)
            println("pathResource1: ${pathResource1.contentLength()}")
            DataBufferUtils.read(pathResource1, dataBufferFactory, 1024).publish().autoConnect()

        })

    }


    fun tile(z: Int, x: Int, y: Int): Flux<DataBuffer> {
        return cache.get(Location(z, x, y))
    }

    data class Location(val z: Int, val x: Int, val y: Int)

}

fun main(args: Array<String>) {
    SpringApplication.run(RadarProxyApplication::class.java, *args)
}
